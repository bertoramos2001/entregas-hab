# entregas-hab

En este repoditorio subiré todas las entregas semanales del bootcamp Hack a Boss

# PROYECTO FINAL (en proceso)
Plataforma donde conectamos jóvenes futbolistas con ojeadores,  teniendo los primeros un catálogo de vídeos subidos por las familias.

## 1.HTML
- **entrega 1:** Creación de un email simple, con imágenes y texto

## 2.JAVASCRIPT 1
- **entrega 1:** Obtener los hashtags en un array de strings

- **entrega 2:** Filtrar objetos para decidir en qué fotografías sale cada persona

- **entrega 3:** Contador de mensajes por usuario y mostrarlos en objetos

## 3.CSS
- **entrega 1:** Creación de una página de reservas de hoteles

## 4.JAVASCRIPT 2
- **entrega 1:** Funcionalidad de añadir hoteles a una lista de reservas en el ejercicio de css y guardarlos en local storage

## 5.BACK
- **entrega 1:** calculadora de préstamo usando la librería loan calc y metiendo los valores por consola

- **entrega 2:** API REST para añadir conciertos y ver la lista de estos

- **entrega 3:** backend de ecommerce; un único usuario (administrador) puede añadir y modificar productos, todos los
                usuarios pueden visualizar la lista de productos. Se pueden filtrar los poroductos mediante la querystring
 >***NOTA*** la contraseña para entrar como asministrador es 1234, como se indica en el código

 >***NOTA*** los objetos se pueden filtrar por nombre, precio mínimo y precio máximo

 ## 6.MySQL
 -**entrega 1:** Base de datos de web que gestiona reservas en restaurantes durante la pandemia

 ## 7.VUE.JS
 -**entrega 1:** Plataforma 'Hack a Music', en la que recibimos datos de la API de Lastfm y los mostramos en diferentes vistas. Entre los datos mostrados se encuentran las canciones y artistas más escuchados en España o los tags de música más utilizados a nivel globals